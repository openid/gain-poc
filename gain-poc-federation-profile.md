%%%
title = "GAIN PoC Federation Profile"
abbrev = "gain-poc-federation"
ipr = "none"
workgroup = "gain-poc"
keyword = ["federation", "openid"]

[seriesInfo]
name = "Internet-Draft"
value = "gain-poc-federation-01"
status = "standard"

[[author]]
initials="D."
surname="Postnikov"
fullname="Dima Postnikov"
organization="Independent"
    [author.address]
    email = "dima@postnikov.net"

%%%

.# Foreword

The OpenID Foundation (OIDF) promotes, protects and nurtures the OpenID community and technologies. As a non-profit international standardizing body, it is comprised by over 160 participating entities (workgroup participant). The work of preparing implementer drafts and final international standards is carried out through OIDF workgroups in accordance with the OpenID Process. Participants interested in a subject for which a workgroup has been established have the right to be represented in that workgroup. International organizations, governmental and non-governmental, in liaison with OIDF, also take part in the work. OIDF collaborates closely with other standardizing bodies in the related fields.

Final drafts adopted by the Workgroup through consensus are circulated publicly for the public review for 60 days and for the OIDF members for voting. Publication as an OIDF Standard requires approval by at least 50% of the members casting a vote. There is a possibility that some of the elements of this document may be the subject to patent rights. OIDF shall not be held responsible for identifying any or all such patent rights.


.# Introduction

This specification is a simplified profile of the OIDC Federation [@!OIDCFED].

.# Warning

This document is not an OIDF International Standard. It is distributed for
review and comment. It is subject to change without notice and may not be
referred to as an International Standard.

Recipients of this draft are invited to submit, with their comments,
notification of any relevant patent rights of which they are aware and to
provide supporting documentation.

.# Notational Conventions

The keywords "shall", "shall not", "should", "should not", "may", and "can" in
this document are to be interpreted as described in ISO Directive Part 2
[@!ISODIR2]. These keywords are not used as dictionary terms such that any
occurrence of them shall be interpreted as keywords and are not to be
interpreted with their natural language meanings.

{mainmatter}

# Scope

This document specifies the requirements for participants of a federation to securely discover each other and establish trust.

# Federation Profile

## Federation configuration
Each network will have implement its own trust anchor (TA).
 
The following entity types are supported for Leafs:
* OP
* RP

Client_id
Client_id is a resolvable URI pointing to entity configuration that has authority_hint pointing to the trust anchor where entity statement (signed by the trust anchorr) can be looked up via fetch endpoint.

## Components
## Entity statement
Entity statement must contain all required field as per [@!OIDCFED] specification.
## Trust chain
TBC

## Metadata

### RP Metadata
 1. client_registration_types shall only support automatic.
 
### OP Metadata
 1. client_registration_types_supported shall only support automatic.
 1. request_authentication_methods_supported shall be defined as in [@!GAINSEC].
 
### OAuth Authorization Server
Not supported
 
### OAuth Client
Not supported
 
### OAuth Protected Resource
Not supported
 
### Federation Entity
All required  properties as defined in [@!OIDCFED] are required.

TBC check optional properties

### Trust Mark Issuer
Not supported

# Federation Policy
### Metadata Policy
As defined in [@!GAINSEC].

### Applying Constraints
1. shall support allowed_leaf_entity_types filter.

### Trust Marks
No trust marks are supported.

## Obtaining Federation Entity Configuration Information
The Entity Configuration of every federation Entity SHALL be exposed at a well-known endpoint. 

## Federation Endpoints
All Federation endpoints are publicly available. 

### Fetching Entity Statements
Fetch endpoint shall be supported.

### Resolve Entity Statement
TBC Resolve endpoint shall be supported 

### Entity Listings
Entity listing endpoint shall be supported. 
 
The following filters / constraints are required to be supported:
* Entity type
 
### Federation Historical Keys endpoint
TBC

## Updating Metadata, Key Rollover, and Revocation

### Revocation
TBC

## OpenID Connect Communication
### Automatic Registration
Only automatic registration shall be supported.

TBC
2 options for OP to choose:
* Complete automatic registration with the real-time entity trust resolution.
or
* OP can rely on background sync with the trust anchor via Listing Endpoint to resolve RP config before RP is trying to connect to RP
 
### Explicit Registration
Explicit registration shall not be supported.

## Security Considerations

No additional security considerations introduced by this profile.

# Privacy considerations

No additional privacy considerations introduced by this profile.

# Acknowledgements

We would like to thank ... for their valuable feedback and contributions that helped to evolve this specification.

{backmatter}

<reference anchor="OIDCFED" target="https://openid.net/specs/openid-connect-federation-1_0.html">
  <front>
    <title>OpenID Connect Federation 1.0 - draft 24</title>
    <author initials="R." surname="Hedberg" fullname="Roland Hedberg">
      <organization>Independent</organization>
    </author>
   <date day="21" month="Oct" year="2022"/>
  </front>
</reference>

<reference anchor="GAINSEC" target="https://bitbucket.org/openid/gain-poc/src/main/gain-poc-security-profile.md">
  <front>
    <title>GAIN Security profile</title>
    <author initials="D." surname="Postnikov" fullname="Dima Postnikov">
      <organization>Independent</organization>
    </author>
   <date day="24" month="Nov" year="2022"/>
  </front>
</reference>


# Notices

Copyright (c) 2022 The OpenID Foundation.

The OpenID Foundation (OIDF) grants to any Contributor, developer, implementer, or other interested party a non-exclusive, royalty free, worldwide copyright license to reproduce, prepare derivative works from, distribute, perform and display, this Implementers Draft or Final Specification solely for the purposes of (i) developing specifications, and (ii) implementing Implementers Drafts and Final Specifications based on such documents, provided that attribution be made to the OIDF as the source of the material, but that such attribution does not indicate an endorsement by the OIDF.

The technology described in this specification was made available from contributions from various sources, including members of the OpenID Foundation and others. Although the OpenID Foundation has taken steps to help ensure that the technology is available for distribution, it takes no position regarding the validity or scope of any intellectual property or other rights that might be claimed to pertain to the implementation or use of the technology described in this specification or the extent to which any license under such rights might or might not be available; neither does it represent that it has made any independent effort to identify any such rights. The OpenID Foundation and the contributors to this specification make no (and hereby expressly disclaim any) warranties (express, implied, or otherwise), including implied warranties of merchantability, non-infringement, fitness for a particular purpose, or title, related to this specification, and the entire risk as to implementing this specification is assumed by the implementer. The OpenID Intellectual Property Rights policy requires contributors to offer a patent promise not to assert certain patent claims against other contributors and against implementers. The OpenID Foundation invites any interested party to bring to its attention any copyrights, patents, patent applications, or other proprietary rights that may cover technology that may be required to practice this specification.
