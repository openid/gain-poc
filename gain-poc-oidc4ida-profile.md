%%%
title = "GAIN PoC OpenID Connect 4 Identity Assurance Profile"
abbrev = "gain-poc-oidc4ida-profile-1_0"
ipr = "none"
area = "Identity"
workgroup = "GAIN PoC"
keyword = ["security", "openid", "gain", "assured identity"]

[seriesInfo]
name = "Internet-Draft"

value = "gain-poc-oidc4ida-profile-00"

status = "standard"

[[author]]
initials="T."
surname="Lodderstedt"
fullname="Torsten Lodderstedt"
organization="yes.com"
    [author.address]
    email = "torsten@lodderstedt.net"

[[author]]
initials="D."
surname="Fett"
fullname="Daniel Fett"
organization="yes.com"
    [author.address]
    email = "mail@danielfett.de"

%%%

.# Abstract

This document defines a profile of OpenID Connect for Identity Assurance [@!OpenID4IDA] for the purpose of the GAIN Proof of Concept.

{mainmatter}

# Introduction {#Introduction}

The GAIN PoC aims at evaluating and demonstrating the technical feasibilty of the vision described in the GAIN whitepaper [@GAIN-WP]. This document defines the technical
interface exposed by IIPs to RPs for the purpose of requesting and receiving assured identity data in the form of verified claims. This interface is a profile of 
the OpenID Connect for Identity Assurance specification. It defines the subset of syntactical elements of `verified_claims` and end-user claims that every compliant 
IIP MUST implement. It also defines the security profile, especially client authentication method, that MUST be utilized by all compliant implementations. 

# Profile Definition
The GAIN PoC uses the technical standard OpenID Connect 4 Identity Assurance [@!OpenID4IDA] for the interface between identity providers and relying parties. This standard supports transfer of  user claims along with metadata about identity assurance levels and trust frameworks. It can be used for integration of "traditional" identity providers but also for wallets and provides an interoperable, easy to integrate interface.

All IIPs MUST at least support the following user claims:

* `given_name`
* `family_name`
* `birthdate`

NOTE: IIPs MAY support further end-user claims, however there is no guarentee for a RPs that all IIPs will support more then the end-user claims listed above.  

End-user claims are requested by the RP using the `claims` as defined in Section 5.5. of [@OpenID] and the `verified_claims` element as defined in [@!OpenID4IDA]. The RP MUST at least request attestation of a `trust_framework` element along with the end-user claims. 

This is a non-normative example request: 

```json
{
   "id_token":{
      "verified_claims":{
         "verification":{
            "trust_framework":null
         },
         "claims":{
            "given_name":null,
            "family_name":null,
            "birthdate":null
         }
      }
   }
}
```
Figure: Example of the `claims` parameter in an OpenID Connect Request

The IIP MUST provid the end-user claims within a `verified_claims` container along with at least a `trust_framework` element as defined in [@!OpenID4IDA]. The trust framework of the particular IIP is conveyed in the `trust_framework` element value. The actual end user claims are provided in the `claims` container. 

This is an example of an ID token issued by an IIP. 

```json
{
   "iss": "https://issuer.example.com",
   "sub": "248289761001",
   "verified_claims": {
       "verification": {
           "trust_framework": "example_trust_framework"
       },
       "claims": {
           "given_name": "Max",
           "family_name": "Meier",
           "birthdate": "1956-01-28"
       }
   }
}
```
Figure: Example ID Token 

NOTE: Every IIP determines the trust framwork it asserts in the verified claims object. RPs use the trust framework to determine how the end user claims were obtained and maintained and whether that is compatible with their requirements. 

OpenID Connect 4 Identity Assurance is used in conjunction with the authorization code grant type as defined in Section 3.1. of [@!OpenID]. 

This profile utilizes certificates for RP authentication and access token replay prevention. RPs authenticate towards IDPs using self-signed X.509 certificates and TLS Client Authentication as defined in [@!RFC8705]. The access tokens issued in this flow MUST be bound to the RP's certificate used to authenticate the client at the respective IIP’s token endpoint (sender constraint access tokens). The IIP must enforce this binding on the userinfo endpoint. 

{backmatter}

<reference anchor="OpenID" target="http://openid.net/specs/openid-connect-core-1_0.html">
  <front>
    <title>OpenID Connect Core 1.0 incorporating errata set 1</title>
    <author initials="N." surname="Sakimura" fullname="Nat Sakimura">
      <organization>NRI</organization>
    </author>
    <author initials="J." surname="Bradley" fullname="John Bradley">
      <organization>Ping Identity</organization>
    </author>
    <author initials="M." surname="Jones" fullname="Mike Jones">
      <organization>Microsoft</organization>
    </author>
    <author initials="B." surname="de Medeiros" fullname="Breno de Medeiros">
      <organization>Google</organization>
    </author>
    <author initials="C." surname="Mortimore" fullname="Chuck Mortimore">
      <organization>Salesforce</organization>
    </author>
   <date day="8" month="Nov" year="2014"/>
  </front>
</reference>

<reference anchor="OpenID-Discovery" target="https://openid.net/specs/openid-connect-discovery-1_0.html">
  <front>
    <title>OpenID Connect Discovery 1.0 incorporating errata set 1</title>
    <author initials="N." surname="Sakimura" fullname="Nat Sakimura">
      <organization>NRI</organization>
    </author>
    <author initials="J." surname="Bradley" fullname="John Bradley">
      <organization>Ping Identity</organization>
    </author>
    <author initials="M." surname="Jones" fullname="Mike Jones">
      <organization>Microsoft</organization>
    </author>
    <author initials="E." surname="Jay" fullname="Edmund Jay">
      <organization>Illumila</organization>
    </author>
   <date day="8" month="Nov" year="2014"/>
  </front>
</reference>

<reference anchor="FAPI-2-BL" target="https://bitbucket.org/openid/fapi/src/master/FAPI_2_0_Baseline_Profile.md">
  <front>
    <title>FAPI 2.0 Baseline Profile </title>
    <author initials="" surname="OpenID Foundation's Financial API (FAPI) Working Group">
      <organization>OpenID Foundation's Financial API (FAPI) Working Group</organization>
    </author>
   <date day="9" month="Sep" year="2020"/>
  </front>
</reference>

<reference anchor="predefined_values_page" target="https://openid.net/wg/ekyc-ida/identifiers/">
  <front>
    <title>Overview page for predefined values</title>
    <author>
      <organization>OpenID Foundation</organization>
    </author>
    <date year="2020"/>
  </front>
</reference>

<reference anchor="OpenID4IDA" target="https://openid.net/specs/openid-connect-4-identity-assurance-1_0-ID3.html">
  <front>
    <title>OpenID Connect for Identity Assurance 1.0</title>
    <author>
      <organization>OpenID Foundation</organization>
    </author>
    <date year="2021" month="09"/>
  </front>
</reference>

<reference anchor="GAIN-WP" target="https://gainforum.org/GAINWhitePaper.pdf">
  <front>
    <title>GAIN DIGITAL TRUST</title>
    <author>
      <organization>over 150 co-authors</organization>
    </author>
    <date year="2021" month="09"/>
  </front>
</reference>

# Acknowledgements {#Acknowledgements}

This specification is based on the work conducted in the GAIN "Alpha PoC" before the OpenID Foundation's GAIN PoC community group was established. The following people contributed to the the initial draft of this profile: Anil Mahalaha, Daniele Citterio, Dima Postnikov, Erik Lupander, Carl Hoessner, Joseph Heenan, Andrea Roeck, Fransceso Vetrano, Maciej Machaluk, Mike Varley. 

We would also like to thank ... for their valuable feedback and contributions that helped to evolve this specification.

# Notices

Copyright (c) 2022 The OpenID Foundation.

The OpenID Foundation (OIDF) grants to any Contributor, developer, implementer, or other interested party a non-exclusive, royalty free, worldwide copyright license to reproduce, prepare derivative works from, distribute, perform and display, this Implementers Draft or Final Specification solely for the purposes of (i) developing specifications, and (ii) implementing Implementers Drafts and Final Specifications based on such documents, provided that attribution be made to the OIDF as the source of the material, but that such attribution does not indicate an endorsement by the OIDF.

The technology described in this specification was made available from contributions from various sources, including members of the OpenID Foundation and others. Although the OpenID Foundation has taken steps to help ensure that the technology is available for distribution, it takes no position regarding the validity or scope of any intellectual property or other rights that might be claimed to pertain to the implementation or use of the technology described in this specification or the extent to which any license under such rights might or might not be available; neither does it represent that it has made any independent effort to identify any such rights. The OpenID Foundation and the contributors to this specification make no (and hereby expressly disclaim any) warranties (express, implied, or otherwise), including implied warranties of merchantability, non-infringement, fitness for a particular purpose, or title, related to this specification, and the entire risk as to implementing this specification is assumed by the implementer. The OpenID Intellectual Property Rights policy requires contributors to offer a patent promise not to assert certain patent claims against other contributors and against implementers. The OpenID Foundation invites any interested party to bring to its attention any copyrights, patents, patent applications, or other proprietary rights that may cover technology that may be required to practice this specification.

# Document History

   -00 (Initial draft of the specification document)

   *  created new document


