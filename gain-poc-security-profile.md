%%%
title = "GAIN PoC Security Profile"
abbrev = "gain-poc-security"
ipr = "none"
workgroup = "gain-poc"
keyword = ["security", "openid"]

[seriesInfo]
name = "Internet-Draft"
value = "gain-poc-security-01"
status = "standard"

[[author]]
initials="D."
surname="Postnikov"
fullname="Dima Postnikov"
organization="Independent"
    [author.address]
    email = "dima@postnikov.net"

%%%

.# Foreword

The OpenID Foundation (OIDF) promotes, protects and nurtures the OpenID community and technologies. As a non-profit international standardizing body, it is comprised by over 160 participating entities (workgroup participant). The work of preparing implementer drafts and final international standards is carried out through OIDF workgroups in accordance with the OpenID Process. Participants interested in a subject for which a workgroup has been established have the right to be represented in that workgroup. International organizations, governmental and non-governmental, in liaison with OIDF, also take part in the work. OIDF collaborates closely with other standardizing bodies in the related fields.

Final drafts adopted by the Workgroup through consensus are circulated publicly for the public review for 60 days and for the OIDF members for voting. Publication as an OIDF Standard requires approval by at least 50% of the members casting a vote. There is a possibility that some of the elements of this document may be the subject to patent rights. OIDF shall not be held responsible for identifying any or all such patent rights.


.# Introduction

This specification is a profile of the FAPI 2.0 Security Profile [@!FAPI2] to be used for GAIN PoC.

.# Warning

This document is not an OIDF International Standard. It is distributed for
review and comment. It is subject to change without notice and may not be
referred to as an International Standard.

Recipients of this draft are invited to submit, with their comments,
notification of any relevant patent rights of which they are aware and to
provide supporting documentation.

.# Notational Conventions

The keywords "shall", "shall not", "should", "should not", "may", and "can" in
this document are to be interpreted as described in ISO Directive Part 2
[@!ISODIR2]. These keywords are not used as dictionary terms such that any
occurrence of them shall be interpreted as keywords and are not to be
interpreted with their natural language meanings.

{mainmatter}

# Scope

This document specifies the requirements for confidential Clients to securely obtain
OAuth tokens from Authorization Servers for the purpose of GAIN PoC.

# Security Profile

## Introduction

This specification is a profile of the FAPI 2.0 Security Profile [@!FAPI2] to be used for GAIN PoC.

## Network Layer Protections

No additional requirements above what is prescribed by [@!FAPI2].
 
## Profile

In the following, a profile of the following technologies is defined:

  * FAPI2 Security profile [@!FAPI2]
  
### Requirements for Authorization Servers

#### General Requirements

Authorization servers, in addition to [@!FAPI2] requirements: 

 1. shall not only issue access or refresh tokens tokens
 1. shall only authenticate GAIN clients using `private_key_jwt` as specified in section 9 of [@!OIDC]

#### Authorization Code Flow

Authorization servers, in addition to [@!FAPI2] requirements: 
 1. shall require a JWS signed JWT request object passed by value with the request parameter or by reference with the request_uri parameter;
 1. shall only use the parameters included in the signed request object passed via the request or request_uri parameter;

Key requirments from [@!FAPI2]:
1. shall support the authorization code grant (`response_type=code` & `grant_type=authorization_code`)
    described in [@!RFC6749]
1. shall support client-authenticated pushed authorization requests
    according to [@!RFC9126]

### Requirements for Clients

#### General Requirements

Clients, in addition to [@!FAPI2] requirements: 

 1. shall not support access or refresh tokens
 1. shall only support client authentication using `private_key_jwt` as specified in section 9 of [@!OIDC]
 1. shall send all parameters inside the authorization request's signed request object

#### Authorization Code Flow

Clients, in addition to [@!FAPI2] requirements: 

 1. shall send all parameters inside the authorization request's signed request object

Key requirments from [@!FAPI2]:
 1. shall use the authorization code grant described in [@!RFC6749]
 1. shall use pushed authorization requests according to [@!RFC9126]

### Requirements for Resource Servers

Resource servers are not utilised at this stage of the GAIN PoC.
    
## Cryptography and Secrets

No additional requirements above what is prescribed by [@!FAPI2].

## Security Considerations

No additional security considerations introduced by this profile.

# Privacy considerations

No additional privacy considerations introduced by this profile.

# Acknowledgements

We would like to thank ... for their valuable feedback and contributions that helped to evolve this specification.

{backmatter}

<reference anchor="FAPI2" target="https://openid.bitbucket.io/fapi/fapi-2_0-security.html">
  <front>
    <title>FAPI 2.0 Security Profile</title>
    <author initials="D." surname="Fett" fullname="Daniel Fett">
      <organization>yes.com</organization>
    </author>
   <date day="23" month="Nov" year="2022"/>
  </front>
</reference>

<reference anchor="OIDC" target="http://openid.net/specs/openid-connect-core-1_0.html">
  <front>
    <title>OpenID Connect Core 1.0 incorporating errata set 1</title>
    <author initials="N." surname="Sakimura" fullname="Nat Sakimura">
      <organization>NRI</organization>
    </author>
    <author initials="J." surname="Bradley" fullname="John Bradley">
      <organization>Ping Identity</organization>
    </author>
    <author initials="M." surname="Jones" fullname="Mike Jones">
      <organization>Microsoft</organization>
    </author>
    <author initials="B." surname="de Medeiros" fullname="Breno de Medeiros">
      <organization>Google</organization>
    </author>
    <author initials="C." surname="Mortimore" fullname="Chuck Mortimore">
      <organization>Salesforce</organization>
    </author>
   <date day="8" month="Nov" year="2014"/>
  </front>
</reference>


# Notices

Copyright (c) 2022 The OpenID Foundation.

The OpenID Foundation (OIDF) grants to any Contributor, developer, implementer, or other interested party a non-exclusive, royalty free, worldwide copyright license to reproduce, prepare derivative works from, distribute, perform and display, this Implementers Draft or Final Specification solely for the purposes of (i) developing specifications, and (ii) implementing Implementers Drafts and Final Specifications based on such documents, provided that attribution be made to the OIDF as the source of the material, but that such attribution does not indicate an endorsement by the OIDF.

The technology described in this specification was made available from contributions from various sources, including members of the OpenID Foundation and others. Although the OpenID Foundation has taken steps to help ensure that the technology is available for distribution, it takes no position regarding the validity or scope of any intellectual property or other rights that might be claimed to pertain to the implementation or use of the technology described in this specification or the extent to which any license under such rights might or might not be available; neither does it represent that it has made any independent effort to identify any such rights. The OpenID Foundation and the contributors to this specification make no (and hereby expressly disclaim any) warranties (express, implied, or otherwise), including implied warranties of merchantability, non-infringement, fitness for a particular purpose, or title, related to this specification, and the entire risk as to implementing this specification is assumed by the implementer. The OpenID Intellectual Property Rights policy requires contributors to offer a patent promise not to assert certain patent claims against other contributors and against implementers. The OpenID Foundation invites any interested party to bring to its attention any copyrights, patents, patent applications, or other proprietary rights that may cover technology that may be required to practice this specification.
