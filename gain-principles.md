%%%
title = "GAIN PoC Principles"
abbrev = "gain-poc-principles-1_0"
ipr = "none"
area = "Identity"
workgroup = "GAIN PoC"
keyword = ["security", "openid", "gain", "assured identity"]

[seriesInfo]
name = "Internet-Draft"

value = "gain-poc-principles-00"

status = "standard"

[[author]]
initials=""
surname=""
fullname=""
organization=""
    [author.address]
    email = ""


%%%

.# Abstract

This document defines high level guiding principles used by GAIN PoC to define PoC's technical requirements and technical solutions.

{mainmatter}

# Introduction {#Introduction}

The GAIN PoC aims at evaluating and demonstrating the technical feasibilty of the vision described in the GAIN whitepaper [@GAIN-WP]. This document defines

# Principles Definition


{backmatter}

<reference anchor="GAIN-WP" target="https://gainforum.org/GAINWhitePaper.pdf">
  <front>
    <title>GAIN DIGITAL TRUST</title>
    <author>
      <organization>over 150 co-authors</organization>
    </author>
    <date year="2021" month="09"/>
  </front>
</reference>

# Acknowledgements {#Acknowledgements}

This specification is based on the work conducted in the GAIN "Alpha PoC" before the OpenID Foundation's GAIN PoC community group was established. The following people contributed to the the initial draft of this profile: Anil Mahalaha, Daniele Citterio, Dima Postnikov, Erik Lupander, Carl Hoessner, Joseph Heenan, Andrea Roeck, Fransceso Vetrano, Maciej Machaluk, Mike Varley. 

We would also like to thank ... for their valuable feedback and contributions that helped to evolve this specification.

# Notices

Copyright (c) 2022 The OpenID Foundation.

The OpenID Foundation (OIDF) grants to any Contributor, developer, implementer, or other interested party a non-exclusive, royalty free, worldwide copyright license to reproduce, prepare derivative works from, distribute, perform and display, this Implementers Draft or Final Specification solely for the purposes of (i) developing specifications, and (ii) implementing Implementers Drafts and Final Specifications based on such documents, provided that attribution be made to the OIDF as the source of the material, but that such attribution does not indicate an endorsement by the OIDF.

The technology described in this specification was made available from contributions from various sources, including members of the OpenID Foundation and others. Although the OpenID Foundation has taken steps to help ensure that the technology is available for distribution, it takes no position regarding the validity or scope of any intellectual property or other rights that might be claimed to pertain to the implementation or use of the technology described in this specification or the extent to which any license under such rights might or might not be available; neither does it represent that it has made any independent effort to identify any such rights. The OpenID Foundation and the contributors to this specification make no (and hereby expressly disclaim any) warranties (express, implied, or otherwise), including implied warranties of merchantability, non-infringement, fitness for a particular purpose, or title, related to this specification, and the entire risk as to implementing this specification is assumed by the implementer. The OpenID Intellectual Property Rights policy requires contributors to offer a patent promise not to assert certain patent claims against other contributors and against implementers. The OpenID Foundation invites any interested party to bring to its attention any copyrights, patents, patent applications, or other proprietary rights that may cover technology that may be required to practice this specification.

# Document History

   -00 (Initial draft of the specification document)

   *  created new document