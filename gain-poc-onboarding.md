# GAIN PoC Technical (IDP) Onboarding

IDPs: Please provide information required to test your IDP implementation: How to onboard, what test data to use, etc.

Note: For the role of the RP, this demo implementation can be used: https://github.com/yescom/gain-poc-examples

## IDP: yes.com Ecosystem

**Description:**

[*yes®*](https://yes.com) is an open banking ecosystem that provides
identity assurance and electronic signature services based on online
banking identities.

**Contact:**

Technical issues: *partner.support\@yes.com*; everything else: Daniel
Fett, *danielf\@yes.com*

**Access:**

yes.com provides the "Test IDP", issuer URL
https://testidp.sandbox.yes.com/issuer/10000002. Credentials are
provided on[
](https://yes.com/docs/rp-devguide/latest/ONBOARDING/index.html)[*this
website*](https://yes.com/docs/rp-devguide/latest/ONBOARDING/index.html).
If you need custom credentials, e.g., a custom redirect URI, please send
a mail to [*partner.support\@yes.com*](mailto:partner.support@yes.com)
with the data provided on the website.

The FAPI2 security profile must be used.

**Test users:**

A range of test user accounts with differing sets of data are available,
e.g.: Peter, Erika, Paula, Hans, Anna (no password required). The user
details available for the respective users are available[
](https://jsonformatter.org/json-pretty-print/?url=https://testidp.sandbox.yes.com/services/userdetails/)[*here*](https://jsonformatter.org/json-pretty-print/?url=https://testidp.sandbox.yes.com/services/userdetails/).

**Note:**

The account chooser described in the documentation can be skipped.
OpenID Connect Discovery starts with the issuer URL of the Test IDP
provided above.


## IDP: BankID Sweden

**Description:**

bankid.com provides identity verification and signature services to
almost every (\> 98%) adult Swedish citizen.

**Contact:**

Technical issues: *teknikinfo\@bankid.com*

(Please include the word GAIN in the subject line)

Everything else: Carl Hössner *carl.hossner\@bankid.com*

**Access:**

OpenID Configuration:

[*https://gain.oidc.pki.nu/.well-known/openid-configuration*](https://gain.oidc.pki.nu/.well-known/openid-configuration)

JWKS endpoint

[*https://gain.oidc.pki.nu/jwks*](https://gain.oidc.pki.nu/jwks)

We'll register RP:s present in this document on or before the 29th of
november.

**Test users:**

In order to integrate with the BankID OpenID Connect Provider för the
GAIN PoC, you will need to obtain a *test *BankID for iOS or Android.
First, you need to download the BankID app from App Store or Google Play
and configure the app for test according to these instructions:

[*https://www.bankid.com/en/utvecklare/test/skaffa-testbankid/testbankid-konfiguration*](https://www.bankid.com/en/utvecklare/test/skaffa-testbankid/testbankid-konfiguration)

Then obtain and install a BankID for test by following the instructions
here:

[*https://www.bankid.com/en/utvecklare/test/skaffa-testbankid/test-bankid-get*](https://www.bankid.com/en/utvecklare/test/skaffa-testbankid/test-bankid-get)

Usage: After your Client has redirected the browser to BankID:s OpenID
Provider and the QR-code is shown, open the BankID app and tap the
\"Scan QR\" button in order to identify yourself.

**Note:**

Note that the timeout of the test system is 30 seconds, so if you\'re
getting errors after entering your PIN, please try again since you need
to both scan the QR code and enter your PIN within 30 seconds.

## IDP: Dizme

**Description:**

[*DIZME*](https://dizme.io) is the first decentralized identity network
that combines the benefits of the SSI world as well as the compliance to
eIDAS regulation perimeter.

**Contact:**

Technical issues: *sergio.shevchenko\@etuitus.it*

All other issues: *daniele.citterio\@infocert.it*

**Access:**

OpenID Configuration:

[*https://gain-oidc-cl.dizme.io/.well-known/openid-configuration*](https://gain-oidc-cl.dizme.io/.well-known/openid-configuration)

**Test users:**

In order to test you need obtain client\_id and secret calling

POST on https://gain-oidc-cl.dizme.io/v2.0/intra-backoffice/client with

-   redirect\_url
-   scope - only openid is supported
-   domain - domain of your service, without https://
-   img\_url - externally available png or jpg image url

this will return you **client\_id** and **secret**.

You will also need to obtain Verified Credential in Dizme App available
on Google Play Store or Apple AppStore. When starting the registration
procedure (on username selection screen, click Advanced), select Stage
Net. Once accessed, you need a Verified Identity credential;

-   if you are an Italian citizen you can add your own id documents
    directly in the wallet.\
    Select Add Credentials and Add Verified Identity

-   otherwise go to[
    ](https://back2work.dizme.io/self_login)[*https://back2work.dizme.io/self\_login*](https://back2work.dizme.io/self_login)
    and scan the QR code to connect to DepoPortal IdP and create a self
    Verified Identity to be used in the Interop Testing environment.

## IDP: Authlete

### Description:

[Authlete](https://www.authlete.com/) is a set of Web APIs with which developers can implement their own authorization servers / OpenID providers that conform to OAuth 2.0, OpenID Connect, FAPI, and other related global standards.

### Contact:

| Purpose   | Email Address        |
|:----------|:---------------------|
| General   | info@authlete.com    |
| Business  | bizdev@authlete.com  |
| Sales     | sales@authlete.com   |
| PR        | pr@authlete.com      |
| Technical | support@authlete.com |

or via the [contact form](https://www.authlete.com/contact/).

### Access:

#### Server Configuration

| Configuration          | Value                                                            |
|:-----------------------|:-----------------------------------------------------------------|
| Issuer Identifier      | https://fapidev-as.authlete.net/                                 |
| Discovery Endpoint     | https://fapidev-as.authlete.net/.well-known/openid-configuration |
| Authorization Endpoint | https://fapidev-www.authlete.net/api/authorization               |
| Token Endpoint         | https://fapidev-as.authlete.net/api/token                        |
| UserInfo Endpoint      | https://fapidev-as.authlete.net/api/userinfo                     |
| JWK Set Document       | https://fapidev-as.authlete.net/api/jwks                         |
| PAR Endpoint           | https://fapidev-as.authlete.net/api/par                          |
| Entity Configuration   | https://fapidev-as.authlete.net/.well-known/openid-federation    |

#### Client Configuration

| Configuration                         | Value                                             |
|:--------------------------------------|:--------------------------------------------------|
| Client ID                             | `5899463614448063`                                |
| Client Type                           | confidential                                      |
| `token_endpoint_auth_method`          | `private_key_jwt`                                 |
| `token_endpoint_auth_signing_alg`     | `RS256`                                           |
| Private Key for Client Authentication | [auth.pri.jwk](credentials/authlete/auth.pri.jwk) |
| Private Key for MTLS                  | [mtls.pri.pem](credentials/authlete/mtls.pri.pem) |
| Self-Signed Certificate for MTLS      | [mtls.cer.pem](credentials/authlete/mtls.cer.pem) |
| Redirect URIs                         | https://fapidev-api.authlete.net/api/mock/redirection/11794872185 <br> http://localhost:3000/yes/oidccb |

#### Federation Relying Party

| Configuration        | Value                                                                             |
|:---------------------|:----------------------------------------------------------------------------------|
| Entity ID            | https://relying-party.authlete.net/5899463614448063                               |
| Entity Configuration | https://relying-party.authlete.net/5899463614448063/.well-known/openid-federation |

The metadata embedded in the entity configuration are metadata of the client
"5899463614448063" which is listed in the "Client Configuration" section above.

#### Trust Anchor

| Configuration        | Value                                                           |
|:---------------------|:----------------------------------------------------------------|
| Entity ID            | https://trust-anchor.authlete.net/                              |
| Entity Configuration | https://trust-anchor.authlete.net/.well-known/openid-federation |
| Fetch Endpoint       | https://trust-anchor.authlete.net/fetch/                        |
| List Endpoint        | https://trust-anchor.authlete.net/list/                         |

### Test users:

| Configuration | Value  |
|:--------------|:-------|
| Login ID      | `inga` |
| Password      | `inga` |
| Datasets      | [document_800_63A.json](https://github.com/authlete/java-oauth-server/blob/master/src/main/resources/ekyc-ida/examples/response/document_800_63A.json) + [document_UKTDIF.json](https://github.com/authlete/java-oauth-server/blob/master/src/main/resources/ekyc-ida/examples/response/document_UKTDIF.json) |

#### Example 1 (Implicit Flow; simplest example)

* [Authorization request with response_type=id_token](https://fapidev-www.authlete.net/api/authorization?client_id=5899463614448063&response_type=id_token&nonce=123456789&scope=openid&redirect_uri=https://fapidev-api.authlete.net/api/mock/redirection/11794872185&claims={"id_token":{"verified_claims":{"verification":{"trust_framework":null},"claims":{"given_name":null,"family_name":null,"birthdate":null,":age_18_or_over":null,"::age_100_or_over":null}}},"transformed_claims":{"age_18_or_over":{"claim":"birthdate","fn":["years_ago",["gte",18]]}}})

#### Example 2 (Authorization Code Flow with PAR and Automatic Registration)

##### Set Parameters

```
ISSUER=https://fapidev-as.authlete.net/
PAR_ENDPOINT=https://fapidev-as.authlete.net/api/par
TOKEN_ENDPOINT=https://fapidev-as.authlete.net/api/token
CLIENT_ID=https://relying-party.authlete.net/5899463614448063
REDIRECT_URI=https://fapidev-api.authlete.net/api/mock/redirection/11794872185
AUTH_PRI=credentials/authlete/auth.pri.jwk
MTLS_PRI=credentials/authlete/mtls.pri.pem
MTLS_CER=credentials/authlete/mtls.cer.pem
```

##### PAR Request

Prepare a client assertion.

```
ASSERTION=`bin/generate-client-assertion.rb --aud=$ISSUER --key=$AUTH_PRI --sub=$CLIENT_ID`
```

Send a PAR request.

```
curl $PAR_ENDPOINT -d client_assertion_type=urn:ietf:params:oauth:client-assertion-type:jwt-bearer -d client_assertion=$ASSERTION -d client_id=$CLIENT_ID -d response_type=code -d scope=openid -d redirect_uri=$REDIRECT_URI --data-urlencode claims='{"id_token":{"verified_claims":{"verification":{"trust_framework":null},"claims":{"given_name":null,"family_name":null,"birthdate":null,":age_18_or_over":null,"::age_100_or_over":null}}},"transformed_claims":{"age_18_or_over":{"claim":"birthdate","fn":["years_ago",["gte",18]]}}}'
```

The JSON below is an example of PAR response.

```json
{
  "expires_in": 90,
  "request_uri": "urn:ietf:params:oauth:request_uri:20auKsHjnMxQg1L23jvvbCDvwlH6MAQk5cTQDO0gPaE"
}
```

The value of the `request_uri` parameter in the PAR response is used in the
following authorization request. Note that the lifetime of the issued request
URI is short, so you must send the authorization request promptly.

##### Authorization Request

Make an authorization request. Don't forget to replace `${CLIENT_ID}` with
the actual client ID (= the entity ID of the relying party) and replace
`${REQUEST_URI}` with the actual request URI issued from the PAR endpoint.

```
https://fapidev-www.authlete.net/api/authorization?client_id=${CLIENT_ID}&request_uri=${REQUEST_URI}
```

Input `inga` and `inga` into the Login ID field and the Password field in the
authorization page.

If you want to make the login form again, please append `&prompt=login` at the
end of the authorization request.

##### Token Request

Make a token request. Don't forget to replace `$AUTHORIZATION_CODE` with the
actual authorization code issued from the authorization endpoint.

```
curl --key $MTLS_PRI --cert $MTLS_CER $TOKEN_ENDPOINT -d client_assertion_type=urn:ietf:params:oauth:client-assertion-type:jwt-bearer -d client_assertion=$ASSERTION -d grant_type=authorization_code -d code=$AUTHORIZATION_CODE -d redirect_uri=$REDIRECT_URI
```

The JSON below is an example of token response.

```json
{
  "access_token":"eyJhbGciOiJQUzI1NiIsInR5cCI6ImF0K2p3dCIsImtpZCI6ImF1dGhsZXRlLWZhcGlkZXYtYXBpLTIwMTgwNTI0In0.eyJzdWIiOiIxMDA0IiwiZ3JhbnRfdHlwZSI6ImF1dGhvcml6YXRpb25fY29kZSIsInNjb3BlIjoib3BlbmlkIiwiYXV0aF90aW1lIjoxNjcwNjA3MTM1LCJpc3MiOiJodHRwczovL2ZhcGlkZXYtYXMuYXV0aGxldGUubmV0LyIsImNuZiI6eyJ4NXQjUzI1NiI6ImhaT05oU01pbE12SkFDLXN5RFlYUk9hOVlIYk8yUXR5bWxSME1LT09HLVUifSwiZXhwIjoxNjcwNjA5NTEyLCJpYXQiOjE2NzA2MDg2MTIsImNsaWVudF9pZCI6Imh0dHBzOi8vcmVseWluZy1wYXJ0eS5hdXRobGV0ZS5uZXQvNTg5OTQ2MzYxNDQ0ODA2MyIsImp0aSI6IlNsM0VEUGpyTXllQkFyaU9NWlFmSjRBSXVkaldNd0lubVR5ck5wajRhN3cifQ.UQu-kO8yo--3dxiiaO2eEnBwNeBxL5LFepzxQWJp8G6k1L0NeVzgF1jMrWfKUSQishnKSSbnQ6ymDjQJ3E7MfJj4vdS8IdMf7wcCsMFTR0UXCpueKgOdXZmWAjU1IWM9rn5rz0jtlw-wNujLlT5sGearta3y8yyvQPgCe7exjdLstwF7Mo6XzXFx28FiZgrLiEouOYOP26QvRcptGeX6kQxiAIeWAxB4YFxlrZgJgmfuuL-mdx4uKp1jkxl0GQ5QZh7MUIvgAlm3od3CMPVDAeSdHQsnIoQPibTbzd069gMqqR9esV-j-x6_OXLv8e1hmlAJ-DjW9vj07BjrQUGvFg",
  "refresh_token":"b172VT2h31P77-0HIiZ_2Nz6844qpZGozyqDFLigpV0",
  "scope":"openid",
  "id_token":"eyJraWQiOiJhdXRobGV0ZS1mYXBpZGV2LWFwaS0yMDE4MDUyNCIsImFsZyI6IlJTMjU2In0.eyJpc3MiOiJodHRwczovL2ZhcGlkZXYtYXMuYXV0aGxldGUubmV0LyIsInN1YiI6IjEwMDQiLCJhdWQiOlsiaHR0cHM6Ly9yZWx5aW5nLXBhcnR5LmF1dGhsZXRlLm5ldC81ODk5NDYzNjE0NDQ4MDYzIl0sImV4cCI6MTY3MDYwODkxMiwiaWF0IjoxNjcwNjA4NjEyLCJhdXRoX3RpbWUiOjE2NzA2MDcxMzUsInZlcmlmaWVkX2NsYWltcyI6eyJ2ZXJpZmljYXRpb24iOnsidHJ1c3RfZnJhbWV3b3JrIjoibmlzdF84MDBfNjNBIn0sImNsYWltcyI6eyJnaXZlbl9uYW1lIjoiSW5nYSIsImZhbWlseV9uYW1lIjoiU2lsdmVyc3RvbmUiLCJiaXJ0aGRhdGUiOiIxOTkxLTExLTA2IiwiOjphZ2VfMTAwX29yX292ZXIiOmZhbHNlLCI6YWdlXzE4X29yX292ZXIiOnRydWV9fX0.PSf0WD8j92e080Jue2LmDeHV7_027OAw9hbnmnJyd26YiA7DCOTiLhlzmKgN_7CPOySkU5I-AWaPeDblVLSrpO1CQpp_GyYHETffC-ew2fIFls59xTUgC81_dBs011VDA95nZc9E3VjrRVcnBrmMWNNaw2plfb6ooXDLj2tTpIoYl_CMbp4eI_1rCo0G0yyZXd3Vw6bTWYKdG33-nnJ7ALO6KtnkcKuIVthFDBFkhA320ujUzopHn7-ksN1onQwQa9bxyFW7XPHp5C24TNsIkBP-an0emPJ0JsSYrYBZFtrxjbuUxZz16SpmTEj3bPZepsSGdfCZCLdbAP_QB4cAwQ",
  "token_type":"Bearer",
  "expires_in":900
}
```

The payload of the ID token in the example token response.

```json
{
  "iss": "https://fapidev-as.authlete.net/",
  "sub": "1004",
  "aud": [
    "https://relying-party.authlete.net/5899463614448063"
  ],
  "exp": 1670608912,
  "iat": 1670608612,
  "auth_time": 1670607135,
  "verified_claims": {
    "verification": {
      "trust_framework": "nist_800_63A"
    },
    "claims": {
      "given_name": "Inga",
      "family_name": "Silverstone",
      "birthdate": "1991-11-06",
      "::age_100_or_over": false,
      ":age_18_or_over": true
    }
  }
}
```

The payload of the access token in the example token response.

```json
{
  "sub": "1004",
  "grant_type": "authorization_code",
  "scope": "openid",
  "auth_time": 1670607135,
  "iss": "https://fapidev-as.authlete.net/",
  "cnf": {
    "x5t#S256": "hZONhSMilMvJAC-syDYXROa9YHbO2QtymlR0MKOOG-U"
  },
  "exp": 1670609512,
  "iat": 1670608612,
  "client_id": "https://relying-party.authlete.net/5899463614448063",
  "jti": "Sl3EDPjrMyeBAriOMZQfJ4AIudjWMwInmTyrNpj4a7w"
}
```

#### Example 3

Another relying party configuration. The same procedure in the Example 2 should work.

```
ISSUER=https://fapidev-as.authlete.net/
PAR_ENDPOINT=https://fapidev-as.authlete.net/api/par
TOKEN_ENDPOINT=https://fapidev-as.authlete.net/api/token
CLIENT_ID=https://relying-party.authlete.net/6298746299842758
REDIRECT_URI=https://fapidev-api.authlete.net/api/mock/redirection/11794872185
AUTH_PRI=credentials/oidc_spa_demo/auth.pri.jwk
MTLS_PRI=credentials/oidc_spa_demo/mtls.pri.pem
MTLS_CER=credentials/oidc_spa_demo/mtls.cer.pem
```

### Resources

* [java-oauth-server](https://github.com/authlete/java-oauth-server/) - Source code of the authorization server
* [OpenID Connect for Identity Assurance, explained by an implementer](https://darutk.medium.com/oidc4ida-93aedffa3058) - Technical article
* [OpenID Connect Federation 1.0](https://www.authlete.com/developers/oidcfed/) - Technical article

## TA/IDP/OP: oidc-federation.online

**Description:**

oidc-federation.online is an indipendent and non-profit project whose purpose is to promote the adoption of OpenID Connect Federation.

**Contact:**

Technical issues: *demarcog83@gmail.com*

**Access:**

**Trust Anchor**

[https://trust-anchor.oidc-federation.online/.well-known/openid-federation](https://trust-anchor.oidc-federation.online/.well-known/openid-federation)

[https://trust-anchor.oidc-federation.online/.well-known/openid-federation?format=json](https://trust-anchor.oidc-federation.online/.well-known/openid-federation?format=json)

**Trust Anchor demo onboarding interface**

It validates the entity configuration and the authority_hints before accepting any submission.

[https://trust-anchor.oidc-federation.online/onboarding/registration/](https://trust-anchor.oidc-federation.online/onboarding/registration/)


**Relying Party**

[https://trust-anchor.oidc-federation.online/oidc/rp/.well-known/openid-federation](https://trust-anchor.oidc-federation.online/oidc/rp/.well-known/openid-federation)

[https://trust-anchor.oidc-federation.online/oidc/rp/.well-known/openid-federation?format=json](https://trust-anchor.oidc-federation.online/oidc/rp/.well-known/openid-federation?format=json)

**Relying Party landing page**

[https://trust-anchor.oidc-federation.online/oidc/rp/landing](https://trust-anchor.oidc-federation.online/oidc/rp/landing)


**OpenID Provider for interop tests**

[https://trust-anchor.oidc-federation.online/oidc/op/.well-known/openid-federation](https://trust-anchor.oidc-federation.online/oidc/op/.well-known/openid-federation)

[https://trust-anchor.oidc-federation.online/oidc/op/.well-known/openid-federation?format=json](https://trust-anchor.oidc-federation.online/oidc/op/.well-known/openid-federation?format=json)


**Test users:**

username: **user**
password: **oidcuser**

**Note:**

oidc-federation.online is based on [spid-cie-oidc-django](https://github.com/italia/spid-cie-oidc-django) and on the italian implementation profile of OpenID Connect Federation and OpenID Connect iGov.

Gain-PoC participants who want to navigate/manage the administration interface (backend) can ask for credentials and a guided tour.