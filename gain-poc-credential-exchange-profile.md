%%%
title = "GAIN PoC Credential Exchange Profile"
abbrev = "gain-poc-credential-exchange-profile"
ipr = "none"
workgroup = "gain-poc"
keyword = ["security", "openid"]

[seriesInfo]
name = "Internet-Draft"
value = ""
status = ""

[[author]]
initials="J."
surname="Vereecken"
fullname="Jan Vereecken"
organization="Meeco"
    [author.address]
    email = "jan.vereecken@meeco.me"

%%%

# GAIN POC Credential Exchange Profile

This document describes the credential exchange profile for stage 1 of the GAIN POC.

## Summary

A summary of the credential profile is provided in the table below

<table>
  <colgroup>
    <col style="width: 25%">
    <col style="width: 50%">
    <col style="width: 25%">
  </colgroup>
  <tr>
    <td><strong>Credential Format</strong></td>
    <td>SD-JWT VC</td>
    <td><a href="https://drafts.oauth.net/oauth-sd-jwt-vc/draft-ietf-oauth-sd-jwt-vc.html">IETF SD-JWT VC (v01)</a></td>
  </tr>
  <tr>
    <td><strong>Signing Algorithm</strong></td>
    <td>ECDSA - Curve P-256 + SHA256 (ES256)</td>
    <td><a href="https://datatracker.ietf.org/doc/html/rfc7518">IETF RFC7518</a></td>
  </tr>
  <tr>
    <td><strong>Key Management (Issuer)</strong></td>
    <td><code>jwt-issuer</code> well-known file</td>
    <td><a href="https://drafts.oauth.net/oauth-sd-jwt-vc/draft-ietf-oauth-sd-jwt-vc.html">IETF SD-JWT VC (v01)</a></td>
  </tr>
  <tr>
    <td><strong>Key Management (Holder)</strong></td>
    <td><code>cnf</code> claim (with JWK key binding)</td>
    <td><a href="https://drafts.oauth.net/oauth-sd-jwt-vc/draft-ietf-oauth-sd-jwt-vc.html">IETF SD-JWT VC (v01)</a></td>
  </tr>
  <tr>
    <td><strong>Issuance</strong></td>
    <td>
      <div class="block">
      <p>OID4VCI</p>
      Pre-Authorized Code Flow
      <ul>
        <li>User Pin</li>
      </ul>
      Authorization Code Flow
      <ul>
        <li>PAR</li>
      </ul>
      Only single, immediate credential issuance supported
      </div>
    </td>
    <td><a href="https://openid.net/specs/openid-4-verifiable-credential-issuance-1_0.html">OIDF OID4VCI (draft 13)</a></td>
  </tr>
</table>

## Credential Format

As credential format, SD-JWT VCs as defined in [SD-JWT-based Verifiable Credentials](https://drafts.oauth.net/oauth-sd-jwt-vc/draft-ietf-oauth-sd-jwt-vc.html) MUST be used.

The following JWT Claims MUST be supported

| Claim | Requirement |
| - | - |
| `iss` | REQUIRED |
| `iat` | REQUIRED |
| `exp` | OPTIONAL |
| `cnf` | REQUIRED |
| `vct` | REQUIRED |

- The claims above MUST NOT be made selectively disclosable by the Issuer, and are always present in the SD-JWT VC presented by the Holder.
- The `exp` claim expresses the validity of an SD-JWT VC and will be present in the SD-JWT VC issued by the Issuer.
- The `iss` claim MUST be an HTTPS URL. This value will later be used to obtain the Issuer's signing key.
- The `cnf` claim MUST include the JSON Web Key in the `jwk` sub claim.

## Key Management

### Issuer Key Management

This profile supports web-based key resolution. The public key used to validate the Issuer’s signature on the SD-JWT VC MUST be obtained from the SD-JWT VC Issuer’s metadata (so called `jwt-issuer` well-known endpoint).

- The JOSE header `kid` MUST be used to identify the respective key.
- Metadata MUST contain either `jwks` or `jwks_uri` to reference the issuer key set.

Below is a non-normative example of `jwt-issuer` metadata including `jwks`.

```json
{
    "issuer": "http://issuer.example.org",
    "jwks": {
      "keys": [
        {
          "kty": "EC",
          "use": "sig",
          "crv": "P-256",
          "kid": "1Jdpq0-Eu0KnZ4R9mapqSiFQfTVvHFg_SrLYifwz8Fc",
          "x": "HniLkguKhi6AJhe3oYTWu5vu0twbtv_CcfXKRJOTBbA",
          "y": "AKZV9n_cXTmW1FcsCDj9Omj4AlWYBv8YOuJQcx967l0",
          "alg": "ES256"
        }
      ]
    }
}
```

### Key Binding

For Cryptographic Key Binding, the `cnf` claim MUST be used. The claim MUST include a JSON Web Key in the `jwk` sub claim.

The following `jwk` claims MUST be supported

| Claim | Requirement |
| - | - |
| `kty` | REQUIRED |
| `crv` | REQUIRED |
| `x` | REQUIRED |
| `y` | REQUIRED |

Below is a non-normative example of the `cnf` claim.

```json
{
  "cnf": {
    "jwk": {
      "kty": "EC",
      "crv": "P-256",
      "x": "TCAER19Zvu3OHF4j4W4vfSVoHIP1ILilDls7vCeGemc",
      "y": "ZxjiWWbZMQGHVWKVQ4hbSIirsVfuecCE6t4jT9F2HZQ"
    }
}
```

## OpenID4VC Credential Format Profile

This section describes how the SD-JWT VC is used in conjunction with OID4VCI.

 The [OpenID4VC High Assurance Interoperability Profile with SD-JWT VC](https://openid.net/specs/openid4vc-high-assurance-interoperability-profile-sd-jwt-vc-1_0-00.html) MUST be adopted. The following sections provide clarification or describe exceptions to the aforementioned profile.

### Credential Example

The SD-JWT VC MUST be composed of the following claims.

| Claim | Type | Requirement |
| - | - | - |
| `vct` | String | REQUIRED |
| `given_name` | String | REQUIRED |
| `family_name` | String | REQUIRED |
| `birthdate` | Date | REQUIRED |

The `birthday` claim is represented in the ISO 8601:2004 `YYYY-MM-DD` format.

Note that this set of claims is intentionally kept small and simple to promote interoperability. Issuers MAY support additional claims, but Wallets and Relying Parties are not required to support these additional claims.

The following is a non-normative example of an unsecured payload of an SD-JWT VC.

```json
{
  "vct": "https://credentials.openid.net/gain-poc-simple-identity-credential",
  "given_name": "Ken",
  "family_name": "Tanaka",
  "birthdate": "1961-12-01"
}
```

The following is a non-normative example of the SD-JWT VC containing only the claims that are selectively disclosable.

```json
{
  "_sd": [
    "710q25t-jjW5M6YJVyoOSoNLUZy5EcnB2ROh0qh90BU",
    "RhEyvDLOOmej5-fCAuYCM4NYt9JqFUah2aasijb_eqk",
    "VIYiRcqKNYV3yv1HTiSMMFLWKXapLKMJEsuGYnvctAA",
    "mIO3775b0cHJbs7BVlAmUjBXBNbf1X65BeFTVxQ8P7k",
    "qy1d_QvBkpQyeS558gDjGwgDTARIlunKDfQJzzLOP2I"
  ],
  "vct": "https://credentials.openid.net/gain-poc-simple-identity-credential",
  "iss": "https://issuer.example.org",
  "iat": 1683000000,
  "exp": 1883000000,
  "_sd_alg": "sha-256",
  "cnf": {
    "jwk": {
      "kty": "EC",
      "crv": "P-256",
      "x": "TCAER19Zvu3OHF4j4W4vfSVoHIP1ILilDls7vCeGemc",
      "y": "ZxjiWWbZMQGHVWKVQ4hbSIirsVfuecCE6t4jT9F2HZQ"
    }
  }
}
```

The following are disclosures belonging to the claims from the above example.

__Claim__ `given_name`:

 * SHA-256 Hash: `mIO3775b0cHJbs7BVlAmUjBXBNbf1X65BeFTVxQ8P7k`
 * Disclosure: `WyJZcFlsNUVuRUNCcTd3Ym5MIiwiZ2l2ZW5fbmFtZSIsIktlbiJd`
 * Contents: ["YpYl5EnECBq7wbnL","given_name","Ken"]


__Claim__ `family_name`:

 * SHA-256 Hash: `RhEyvDLOOmej5-fCAuYCM4NYt9JqFUah2aasijb_eqk`
 * Disclosure: `WyJobGQ1eHN6bnRFNTNKdnB4IiwiZmFtaWx5X25hbWUiLCJUYW5ha2EiXQ`
 * Contents: ["hld5xszntE53Jvpx","family_name","Tanaka"]


__Claim__ `birthdate`:

 * SHA-256 Hash: `VIYiRcqKNYV3yv1HTiSMMFLWKXapLKMJEsuGYnvctAA`
 * Disclosure: `WyJiQVZqa1REM1kwazFHdkNNIiwiYmlydGhkYXRlIiwiMTk2MS0xMi0wMSJd`
 * Contents: ["bAVjkTD3Y0k1GvCM","birthdate","1961-12-01"]

Note that the above example SD-JWT also contains two decoy digests.

The following is a non-normative example of the secured SD-JWT VC containing all the claims and disclosures.

```
eyJ0eXAiOiJ2YytzZC1qd3QiLCJhbGciOiJFUzI1NiJ9.eyJpYXQiOjE2ODMwMDAwMDAsImV4cCI6MTg4MzAwMDAwMCwiY25mIjp7Imp3ayI6eyJrdHkiOiJFQyIsImNydiI6IlAtMjU2IiwieCI6IlRDQUVSMTladnUzT0hGNGo0VzR2ZlNWb0hJUDFJTGlsRGxzN3ZDZUdlbWMiLCJ5IjoiWnhqaVdXYlpNUUdIVldLVlE0aGJTSWlyc1ZmdWVjQ0U2dDRqVDlGMkhaUSJ9fSwiaXNzIjoiaHR0cHM6Ly9pc3N1ZXIuZXhhbXBsZS5vcmciLCJ2Y3QiOiJodHRwczovL2NyZWRlbnRpYWxzLm9wZW5pZC5uZXQvZ2Fpbi1wb2Mtc2ltcGxlLWlkZW50aXR5LWNyZWRlbnRpYWwiLCJfc2QiOlsiNzEwcTI1dC1qalc1TTZZSlZ5b09Tb05MVVp5NUVjbkIyUk9oMHFoOTBCVSIsIlJoRXl2RExPT21lajUtZkNBdVlDTTROWXQ5SnFGVWFoMmFhc2lqYl9lcWsiLCJWSVlpUmNxS05ZVjN5djFIVGlTTU1GTFdLWGFwTEtNSkVzdUdZbnZjdEFBIiwibUlPMzc3NWIwY0hKYnM3QlZsQW1VakJYQk5iZjFYNjVCZUZUVnhROFA3ayIsInF5MWRfUXZCa3BReWVTNTU4Z0RqR3dnRFRBUklsdW5LRGZRSnp6TE9QMkkiXSwiX3NkX2FsZyI6InNoYTI1NiJ9.APaqMkvZiBYwVnStxIernXx0n6FsK4vi9geai3m_aLGehPW_wZgxl_aGW5YlZifDQuoWA8HvPDMHIQ9pYrVSdg~WyJZcFlsNUVuRUNCcTd3Ym5MIiwiZ2l2ZW5fbmFtZSIsIktlbiJd~WyJobGQ1eHN6bnRFNTNKdnB4IiwiZmFtaWx5X25hbWUiLCJUYW5ha2EiXQ~WyJiQVZqa1REM1kwazFHdkNNIiwiYmlydGhkYXRlIiwiMTk2MS0xMi0wMSJd~
```

## Signing Algorithm

Issuers, holders and verifiers MUST support P-256 (secp256r1) as a key type with ES256 JWT algorithm for signing and signature validation whenever this profiles requires. This includes

- SD JWT-VC
- Key Binding JWT (Holder)
- PAR Request Object

## OpenID for Verifiable Credential Issuance

Implementations of this profile

- MUST support the pre-authorization code flow and authorization code flow.
- MUST support immediate issuance
- MUST support credential offer for pre-authorized code flow and authorization code flow
- MAY support wallet initiated credential issuance for authorization code flow

## Credential Offer

- MUST contain credential identifier that references the item in the `credential_supported` map that matches the credential in section, "Credential Example"

It is recommended that a webpage is provided by Issuers to allow Wallet providers to initiate credential issuance through a credential offer. The webpage:

- MAY contain input for Credential Offer URI.
- MAY have the option to include pre-authorized code grant
  - MAY contain possibility to select user pin
  - MAY input user pin length
- MAY have the option to include authorization code grant

## Token Endpoint

Wallets can send attestation for client authentication, but these can be ignored by the Issuer.

### Pre-Authorized Code Flow

![](diagrams/gain-poc-oid4vci-pre-auth.png)

### Authorization Code Flow

![](diagrams/gain-poc-oid4vci-auth-code.png)

## Document History

-01

- Add sequence diagrams for authorization code flow and pre-authorized code flow
- Updated link to OpenIDVC HAIP

-00

- Initial version
- Add version to SD-JWT VC and OID4VCI
- Add credential example
- Reference OpenID4VC HAIP with SD-JWT VC section 7.2 as the credential profile instead of defining a new one
