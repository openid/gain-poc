%%%
title = "GAIN PoC Requirements"
abbrev = "gain-poc-requirements-1_0"
ipr = "none"
area = "Identity"
workgroup = "GAIN PoC"
keyword = ["security", "openid", "gain", "assured identity"]

[seriesInfo]
name = "Internet-Draft"

value = "gain-poc-requirements-00"

status = "standard"

[[author]]
initials="M."
surname="Haine"
fullname="Mark Haine"
organization="Considrd.Consulting"
    [author.address]
    email = "mark@considrd.consulting"

[[author]]
initials="D."
surname="Postnikov"
fullname="Dima Postnikov"
    [author.address]
    email = "dima@postnikov.net"

%%%

.# Abstract

This document describes a set of draft requirements for the GAIN Proof of Concept.

{mainmatter}

# Introduction {#Introduction}

The GAIN PoC aims at evaluating and demonstrating the technical feasibilty of the vision described in the GAIN whitepaper [@GAIN-WP].

During the GAIN POC meeting 28/01/2022 there was agreement that the first step toward candidate solutions for the participant directory and IDP chooser were a high level list of requirements.  This document is for POC contributors to collect initial thoughts about this.  
The intent is to be brief at this stage so we are hoping for bullet points rather than detailed definitions.

# Architecture

## Architecture Context

The initial architecture context for GAIN is shown in the diagram below and the scope for the next phase of the GAIN PoC will be the box labeled “Ecosystem federation profile, registration, discovery and management” in the bottom left corner.

** INSERT OR LINK Architecture Context diagram here **

## Initial Hypothesis

The diagram below shows the starting hypothesis for the architecture.  There are two ew components envisioned the first being the “RP & IDP Metadata component” and the second being the “IDP chooser”.

The assumption is that the “IDP chooser” will be an application that consumes and applies logic to data provided by the “RP & IDP Metadata component”

A chooser application consumes data to help RPs and their end-users to select IDP/IIP there are expected to be many instances of this.
Might integrate with shared “preferences store” for end user preferences sharing (to improve UX) and only if privacy and security concerns can be mitigated sufficiently.

** INSERT OR LINK Architecture Hypothesis diagram here **

## GAIN Trust On-boarding 

It is anticipated that many Governance bodies or their agents or other intermediary will need to be supported to allow for many national or industry aligned groups to join GAIN.

** INSERT OR LINK On-boarding diagram here **

## GAIN Trust Runtime 

** INSERT OR LINK Runtime diagram here **

## Functional Requirements

__Network trust establishment__

* Establish closed circle of "trusted" participants.
* Establish a way for all participants to understand that another participant is “trusted” and a part of the network.
* Participant on-boarding.
* Participant information.
* Participant off-boarding.
* Participant suspension or revocation (by network operator or on participant's request).
* Allow for participation of different type of entities (identity information provider, relying party, service provider and etc.).
* Authentication lifecycle management (e.g. key rotation)

__Participant discovery__

* Participant directory (list with filters by type, by country, by feature / capability). As a RP, I want to find out whether a certain IDP supports my use case: * Find out capabilities of other participants (including supported interfaces/protocols). This should globally available and distributed data service.
* Participant metadata lookup from from a directory. This should globally available and distributed data service.
* Participant metadata discovery from a participant.
* Pricing discovery (potentially with differing pricing per attribute, bundle of attributes, market, or other dimensions). Suggestion that commercial arrangement metadata should be kept confidential.

__Transaction__

* Authentication of participants (to the register and to each other).
* IDP chooser component (optional).
* Transaction information capture for billing purposes.

__Post-transaction__

* Billing.
* Investigation and tracing.

__Operations and Support__

* Availability monitoring could be part of this or separate.
* Participant support.
* End-user support.

__Key Journeys__

* As a newly authorized IDP/IIP I need to have my metadata added to the metadata directory.
* As a newly authorized RP I need to have my metadata added to the metadata directory.
* As a network member I need to be confident that changes to another organization’s metadata are made in a clearly defined timely fashion.
* As a network member I need to be confident that removals of other organization’s metadata is removed when necessary and in a clearly defined timely fashion.

# Data Model

## Member Entity Attributes

* Identifier
* Human readable name
* Commercial terms
* Credential
* Roles
* Permissions
* Status - pending, active, past

## Public Data

* Name of organization
* Org identifier
* credential/Key material
* Support contact details
* Regulatory status/Role

## Restricted Data

* Commercial terms
* Credential verification (public key?)
* App identification


## Non-Functional Requirements - GAIN RP & IDP Metadata

* Availability
* Performance
* Scalability
* RTO
* RPO
* Data retention times and traceability
* Compliance
* Cost
* Security

Question: Is there data that needs to be kept confidential?

## Functional Requirements - GAIN IDP Chooser

Select / Filter IDPs driven by RP on based upon:

* The required Level of Assurance
* Attributes’ availability
* IDP availability
* Geography
* Cookies in user agent to remember previous choice
* Support multiple favourites - that persists across multiple RP

## User Experience guidelines 

* Nascar problem
* How to choose the IDP/IIP
* Autocomplete with fuzzy/alternate name match

{backmatter}

<reference anchor="OpenID" target="http://openid.net/specs/openid-connect-core-1_0.html">
  <front>
    <title>OpenID Connect Core 1.0 incorporating errata set 1</title>
    <author initials="N." surname="Sakimura" fullname="Nat Sakimura">
      <organization>NRI</organization>
    </author>
    <author initials="J." surname="Bradley" fullname="John Bradley">
      <organization>Ping Identity</organization>
    </author>
    <author initials="M." surname="Jones" fullname="Mike Jones">
      <organization>Microsoft</organization>
    </author>
    <author initials="B." surname="de Medeiros" fullname="Breno de Medeiros">
      <organization>Google</organization>
    </author>
    <author initials="C." surname="Mortimore" fullname="Chuck Mortimore">
      <organization>Salesforce</organization>
    </author>
   <date day="8" month="Nov" year="2014"/>
  </front>
</reference>

<reference anchor="OpenID-Discovery" target="https://openid.net/specs/openid-connect-discovery-1_0.html">
  <front>
    <title>OpenID Connect Discovery 1.0 incorporating errata set 1</title>
    <author initials="N." surname="Sakimura" fullname="Nat Sakimura">
      <organization>NRI</organization>
    </author>
    <author initials="J." surname="Bradley" fullname="John Bradley">
      <organization>Ping Identity</organization>
    </author>
    <author initials="M." surname="Jones" fullname="Mike Jones">
      <organization>Microsoft</organization>
    </author>
    <author initials="E." surname="Jay" fullname="Edmund Jay">
      <organization>Illumila</organization>
    </author>
   <date day="8" month="Nov" year="2014"/>
  </front>
</reference>

<reference anchor="FAPI-2-BL" target="https://bitbucket.org/openid/fapi/src/master/FAPI_2_0_Baseline_Profile.md">
  <front>
    <title>FAPI 2.0 Baseline Profile </title>
    <author initials="" surname="OpenID Foundation's Financial API (FAPI) Working Group">
      <organization>OpenID Foundation's Financial API (FAPI) Working Group</organization>
    </author>
   <date day="9" month="Sep" year="2020"/>
  </front>
</reference>

<reference anchor="predefined_values_page" target="https://openid.net/wg/ekyc-ida/identifiers/">
  <front>
    <title>Overview page for predefined values</title>
    <author>
      <organization>OpenID Foundation</organization>
    </author>
    <date year="2020"/>
  </front>
</reference>

<reference anchor="OpenID4IDA" target="https://openid.net/specs/openid-connect-4-identity-assurance-1_0-ID3.html">
  <front>
    <title>OpenID Connect for Identity Assurance 1.0</title>
    <author>
      <organization>OpenID Foundation</organization>
    </author>
    <date year="2021" month="09"/>
  </front>
</reference>

<reference anchor="GAIN-WP" target="https://gainforum.org/GAINWhitePaper.pdf">
  <front>
    <title>GAIN DIGITAL TRUST</title>
    <author>
      <organization>over 150 co-authors</organization>
    </author>
    <date year="2021" month="09"/>
  </front>
</reference>

# Acknowledgements {#Acknowledgements}

This specification is based on the work conducted in the GAIN "Alpha PoC" before the OpenID Foundation's GAIN PoC community group was established. The following people contributed to the the initial draft of this profile: Torsten Lodderstedt, Dima Postnikov, Mark Haine, Carl Hössner, 
We would also like to thank ... for their valuable feedback and contributions that helped to evolve this specification.

# Notices

Copyright (c) 2022 The OpenID Foundation.

The OpenID Foundation (OIDF) grants to any Contributor, developer, implementer, or other interested party a non-exclusive, royalty free, worldwide copyright license to reproduce, prepare derivative works from, distribute, perform and display, this Implementers Draft or Final Specification solely for the purposes of (i) developing specifications, and (ii) implementing Implementers Drafts and Final Specifications based on such documents, provided that attribution be made to the OIDF as the source of the material, but that such attribution does not indicate an endorsement by the OIDF.

The technology described in this specification was made available from contributions from various sources, including members of the OpenID Foundation and others. Although the OpenID Foundation has taken steps to help ensure that the technology is available for distribution, it takes no position regarding the validity or scope of any intellectual property or other rights that might be claimed to pertain to the implementation or use of the technology described in this specification or the extent to which any license under such rights might or might not be available; neither does it represent that it has made any independent effort to identify any such rights. The OpenID Foundation and the contributors to this specification make no (and hereby expressly disclaim any) warranties (express, implied, or otherwise), including implied warranties of merchantability, non-infringement, fitness for a particular purpose, or title, related to this specification, and the entire risk as to implementing this specification is assumed by the implementer. The OpenID Intellectual Property Rights policy requires contributors to offer a patent promise not to assert certain patent claims against other contributors and against implementers. The OpenID Foundation invites any interested party to bring to its attention any copyrights, patents, patent applications, or other proprietary rights that may cover technology that may be required to practice this specification.

# Document History

   -00 (Initial draft of the requirements document)

   *  created new document


